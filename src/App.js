import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className='container'>
        <div className='flex-container'>
          {<Cards title='Матрица (Matrix)' country='США' year='1999' rates='8.491' link='/images/matrix.jpg' />}
          {<Cards title='Побег из Шоушенка (The Shawshank Redemption)' country='США' year='1994' rates='9.113' link='/images/shawshank.jpg' />}
          {<Cards title='Форрест Гамп (Forrest Gump)' country='США' year='1994' rates='8.920' link='/images/forrest-gump.jpg' />}
          {<Cards title='Бриллиантовая рука' country='СССР' year='1968' rates='8,528' link='/images/diamond-hand.jpg' />}
          {<Cards title='Бойцовский клуб (Fight Club)' country='США, Германия' year='1999' rates='8,655' link='/images/fight-club.jpg' />}
          {<Cards title='Крестный отец (The Godfather)' country='США' year='1972' rates='8,736' link='/images/god-father.jpg' />}
          {<Cards title='Зеленая миля (The Green Mile)' country='США' year='1999' rates='9,063' link='/images/green-mile.jpg' />}
          {<Cards title='Достучаться до небес (Knockin on Heaven"s Door)' country='Германия' year='1997' rates='8,633' link='/images/knock.jpg' />}
          {<Cards title='Операция "Ы"' country='СССР' year='1965' rates='8,609' link='/images/operation.jpg' />}
          {<Cards title='Криминальное чтиво (Pulp Fiction)' country='США' year='1994' rates='8,614' link='/images/pulp-fiction.jpg' />}
          {<Cards title='Рокки (Rocky)' country='США' year='1976' rates='8,015' link='/images/rocky.jpg' />}
          {<Cards title='Звездные войны (Star wars)' country='США' year='1977' rates='8,114' link='/images/star-wars.jpg' />}
        </div>
      </div>
    );
  }
}

function Cards(props) {
  return (
    <div className='card'>
      <img src={props.link} alt="Poster" width="100px" height="150px"/>
      <h1>{props.title}</h1>
      <p>Год выпуска: {props.year}</p>
      <p>Страна: {props.country}</p>
      <p>Рейтинг: {props.rates}</p>
    </div>
  )
}

export default App;
